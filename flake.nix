{
  description = "Max Hero's Flake to Work on SalesRabbit";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixpkgs-master.url = "github:NixOS/nixpkgs";
    nix-doom-emacs.url = "github:nix-community/nix-doom-emacs";
    nix-doom-emacs.inputs.nixpkgs.follows = "nixpkgs";
    maxhero-flake.url = "github:themaxhero/.dotfiles";
    maxhero-flake.inputs.nixpkgs.follows = "nixpkgs";
    maxhero-flake.inputs.nix-doom-emacs.follows = "nix-doom-emacs";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    devshell.url = "github:numtide/devshell";
  };

  outputs = { self, nixpkgs, ... }@attrs: {
    formatter.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.nixfmt;
    nixosConfigurations.nixos-vm = import ./nixos-vm attrs;
  };
}
