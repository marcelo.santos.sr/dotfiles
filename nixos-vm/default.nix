{ self, nixpkgs, home-manager, maxhero-flake, nix-doom-emacs, ... }@attrs:
nixpkgs.lib.nixosSystem {
  system = "x86_64-linux";
  specialArgs = attrs;
  modules = [
    ./configuration.nix
    ./hardware-configuration.nix
    home-manager.nixosModules.home-manager
    ({ config, pkgs, ... }: {
      home-manager = {
        useGlobalPkgs = true;
        useUserPackages = true;
        users.maxhero = maxhero-flake.mkHome {
          username = "maxhero";
          homeDirectory = "/home/maxhero";
          enableDoomEmacs = false;
          enableDevelopment = true;
          enableGaming = false;
          personal = false;
          enableUI = true;
          extraPackages = with pkgs; [
            openshift
            ocm
            asdf-vm
          ];
          extraModules = [
            ({ ... }: {
              programs.git = {
                enable = true;
                userName = "Marcelo Amancio de Lima Santos";
                userEmail = "marcelo.santos@salesrabbit.com";
                extraConfig = {
                  rerere.enabled = true;
                  pull.rebase = true;
                  tag.gpgsign = true;
                  init.defaultBranch = "master";
                  core = {
                    excludefile = "$NIXOS_CONFIG_DIR/scripts/gitignore";
                    editor = "vim";
                  };
                };
              };
              programs.home-manager.enable = true;
              xdg.configFile."efm-langserver/config.yaml".text = import ../efm_ls/config.yaml.nix attrs; 
            })
          ];
        };
        extraSpecialArgs = attrs // {
          inherit nix-doom-emacs;
          self = maxhero-flake;
          nixosConfig = config;
        };
      };
    })
  ];
}
