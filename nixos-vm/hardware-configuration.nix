# Do not modify this file!  It was generated by ‘nixos-generate-config’
# and may be overwritten by future invocations.  Please make changes
# to /etc/nixos/configuration.nix instead.
{ config, lib, pkgs, modulesPath, ... }: {
  imports = [ ];

  boot.initrd.availableKernelModules =
    [ "ata_piix" "mptspi" "uhci_hcd" "ehci_pci" "sd_mod" "sr_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ ];
  boot.extraModulePackages = [ ];

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/87fb06b4-26aa-4837-b8a4-e6c8f3251fc9";
    fsType = "ext4";
  };

  boot.initrd.luks.devices."luks-e5ce8f3a-5538-43fd-b50b-b026d0e69533".device =
    "/dev/disk/by-uuid/e5ce8f3a-5538-43fd-b50b-b026d0e69533";

  swapDevices =
    [{ device = "/dev/disk/by-uuid/00f9505e-e017-46a8-94a1-b30ee6dfaac2"; }];

  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.ens33.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
}
