{ pkgs, ... }:
let
  marketplace-extensions = pkgs.vscode-utils.extensionsFromVscodeMarketplace [
    {
      name = "remote-ssh-edit";
      publisher = "ms-vscode-remote";
      version = "0.47.2";
      sha256 = "1hp6gjh4xp2m1xlm1jsdzxw9d8frkiidhph6nvl24d0h8z34w49g";
    }
    {
      name = "gitpod-desktop";
      publisher = "gitpod";
      version = "0.0.152";
      sha256 = "kLPLqiNnNFwGiyxA60UwElvk5r0Dv6gL4nZt7WfgOCQ=";
    }
    {
      name = "gitpod-remote-ssh";
      publisher = "gitpod";
      version = "0.0.49";
      sha256 = "jUwsMukT5hRo9JvD1m4sg5XAyTxynLj2Dq+kXRZ9bpY=";
    }
  ];
in
pkgs.vscode-with-extensions.override {
  vscodeExtensions = with pkgs.vscode-extensions;
    [
      bbenoist.nix
      ms-python.python
      ms-azuretools.vscode-docker
      ms-vscode-remote.remote-ssh
      ms-vsliveshare.vsliveshare
      elixir-lsp.vscode-elixir-ls
      phoenixframework.phoenix
      elmtooling.elm-ls-vscode
      mkhl.direnv
      tabnine.tabnine-vscode
      vscodevim.vim
      rust-lang.rust-analyzer
      redhat.vscode-yaml
      redhat.vscode-xml
      prisma.prisma
      ocamllabs.ocaml-platform
      ms-vscode.makefile-tools
      ms-vscode.live-server
      ms-vscode.hexeditor
    ] ++ marketplace-extensions;
}
